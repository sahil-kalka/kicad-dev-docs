---
title: Compile Options
weight: 17
summary: Summary of compile options via CMake Config
---

These are options that are passed to cmake during configuration

== All Platforms


[cols="4,12,1",options="header"]
|===
|Option                             | Description                                               | Default
|KICAD_SCRIPTING_WXPYTHON           | Build wxPython implementation for wx interface building in Python and py.shell.
                                                                                                | ON
|KICAD_USE_OCE                      | Build tools and plugins related to OpenCascade Community Edition. +
                                        Needed to support import/export STEP
                                                                                                | OFF
|KICAD_USE_OCC                      | Build tools and plugins related to OpenCascade Technology. +
                                      Overrides KICAD_USE_OCE
                                                                                                | ON
|KICAD_INSTALL_DEMOS                | Install KiCad demos and examples.                         | ON
|KICAD_BUILD_QA_TESTS               | Build software Quality assurance unit tests.              | ON
|KICAD_SPICE                        | Build KiCad with internal Spice simulator.                | ON
|KICAD_BUILD_I18N                   | Build the translation language libraries                  | OFF
|KICAD_I18N_UNIX_STRICT_PATH        | Install the language libraries to the standard UNIX install path +
                                      of ${CMAKE_INSTALL_PREFIX}/share/locale                   | OFF
|BUILD_SMALL_DEBUG_FILES            | In debug build: create smaller binaries. +
                                      On Windows, binaries created by link option -g3 are *very large* +
                                      (more than 1Gb for Pcbnew, and more than 3Gb for the full KiCad suite) +
                                      This option create binaries using link option -g1 that create much smaller files, but +
                                      there are less info in debug (However the file names and line numbers are available) +
                                                                                                | OFF
|MAINTAIN_PNGS                      | Allow build/rebuild bitmap icons used in menus from the corresponding .svg file. +
                                      Set to true if you are a PNG maintainer and have the required tools given +
                                      in the bitmaps_png/CMakeLists.txt file
                                                                                                | OFF

|===


== Not supported by all platforms


[cols="4,12,1",options="header"]
|===
|Option                             | Description                                               | Default
|KICAD_SANITIZE                     | Build KiCad with sanitizer options. +
                                      WARNING: Not compatible with gold linker.
                                                                                                | OFF
|KICAD_STDLIB_DEBUG                 | Build KiCad with libstdc++ debug flags enabled.           | OFF
|KICAD_STDLIB_LIGHT_DEBUG           | Build KiCad with libstdc++ with -Wp,-D_GLIBCXX_ASSERTIONS flag enabled. +
                                      Not as intrusive as KICAD_STDLIB_DEBUG
                                                                                                | OFF
|KICAD_BUILD_PARALLEL_CL_MP         | Build in parallel using the /MP compiler option (Default OFF for safety reasons).
                                                                                                 | OFF
|KICAD_USE_VALGRIND                 | Build KiCad with valgrind stack tracking enabled.          | OFF
|===


== Notes

=== Note 1

Python 3 is required to build KiCad.  The path to Python is normally determined automatically by a
CMake script, but if needed, `PYTHON_EXECUTABLE` can be defined when invoking cmake
( use `-DPYTHON_EXECUTABLE=<python path>` ) to specify a particular Python binary.

=== Note 2

These Symbols are always defined, and are not an option for cmake invocation:


*COMPILING_DLL*

This is a signal to import_export.h, and when present, toggles the
interpretation of the #defines in that file. Its purpose should not be
extended beyond this.


*USE_KIWAY_DLLS*

Comes from CMake as a user configuration variable, settable in the Cmake
user interface. It decides if KiCad will be built with the *.kiface program
modules.


*BUILD_KIWAY_DLL*

Comes from CMake, but at the 2nd tier, not the top tier. By 2nd tier,
something like pcbnew/CMakeLists.txt, not /CMakeLists.txt is meant. It is
not a user configuration variable. Instead, the 2nd tier CMakeLists.txt file
looks at the top level `USE_KIWAY_DLLS` and decides how the object files under
the 2nd tier's control will be built. If it decides it wants to march in
lockstep with `USE_KIWAY_DLLS`, then this local CMakeLists.txt file may pass a
defined `BUILD_KIWAY_DLL` (singular) on the compiler command line to the
pertinent set of compilation steps under its control.

=== Note 3

When building with `KICAD_BUILD_I18N` on Linux systems, gettext needs the rule files
`shared-mime-info.its` and `metainfo.its`/`appdata.its` to translate the Linux
metadata files.
